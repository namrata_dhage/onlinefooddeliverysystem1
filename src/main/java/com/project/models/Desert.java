package com.project.models;

import javax.persistence.*;

@Entity
@Table(name = "deserts")
public class Desert {

	@Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long des_id;
	private String Desert_name;
	
	private String category;
	
	private float price;
	private String description;
		
	public Desert() {
		
	}	
	
	public Desert(String desert_name, String category, String images) {
		super();
		Desert_name = desert_name;
		this.category = category;
		this.images = images;
	}
	@Lob
	@Column(name="images",nullable = true, length = 45)
	private String images;

	public Long getDes_id() {
		return des_id;
	}

	public void setDes_id(Long des_id) {
		this.des_id = des_id;
	}

	public String getDesert_name() {
		return Desert_name;
	}

	public void setDesert_name(String desert_name) {
		Desert_name = desert_name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}
	
	@Transient 
	public String getImagesImagePath() {
		if(images == null || des_id == null)
			return null;
		
		return "./desertImg-photos/" +des_id + "/" +images;
	}
	
	@Override
	public String toString() {
		return "Desert [des_id=" + des_id + ", Desert_name=" + Desert_name + ", category=" + category + ", price="
				+ price + ", description=" + description + ", images=" + images + "]";
	}
	
}
