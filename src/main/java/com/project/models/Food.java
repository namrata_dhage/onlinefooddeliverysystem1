package com.project.models;


import javax.persistence.*;

@Entity
@Table(name = "foods")
public class Food {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long food_id;

	private String food_name;
	
	private String category;

	private float price;
	private String description;
	
	@Column(nullable = true, length = 64)
	private String images;

	public String getImages() {
		return images;
	}

	public void setImage(String images) {
		this.images = images;
	}

	public Long getFood_id() {
		return food_id;
	}

	public void setFood_id(Long food_id) {
		this.food_id = food_id;
	}

	public String getFood_name() {
		return food_name;
	}

	public void setFood_name(String food_name) {
		this.food_name = food_name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	@Transient 
	public String getImagesImagePath() {
		if(images == null || food_id == null)
			return null;
		
		return "./foodImg-photos/" +food_id + "/" +images;
	}
	

	@Override
	public String toString() {
		return "Food [food_id=" + food_id + ", food_name=" + food_name + ", price=" + price + ", description="
				+ description + ", images=" + images + "]";
	}
}
