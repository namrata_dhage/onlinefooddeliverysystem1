package com.project.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.lang.NonNull;

@Entity
@Table(name="customers")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable = false,length = 45)
	private String Fname;
	@Column(nullable = false,length = 65)
	private String Lname;
	@Column(nullable = false,unique = true,length = 65)
	
	@NonNull
	
	private String email;
	@Column(name="",nullable = false,length = 65)
	private String password;
	@Column(nullable = false,length = 65)
	private String Address;
	@Column(nullable = false,length = 65)
	private String city;
	@Column(nullable = false,length = 65)
	private int contact_no;
	
	@OneToMany(targetEntity = Food.class,cascade = CascadeType.ALL)
	@JoinColumn(name="cust_id", referencedColumnName ="id" )
	private List<Food> foods;
	
	@OneToMany(targetEntity = Desert.class,cascade = CascadeType.ALL)
	@JoinColumn(name="cust_id",referencedColumnName = "id")
	private List<Desert> deserts = new ArrayList<>();
	
	
	 @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	    @JoinTable(
	            name = "users_roles",
	            joinColumns = @JoinColumn(name = "user_id"),
	            inverseJoinColumns = @JoinColumn(name = "role_id")
	            )
	 private Set<Role> roles = new HashSet<>();
	 
	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}	
	public Customer(Long id, String fname, List<Food> foods) {
		super();
		this.id = id;
		Fname = fname;
		this.foods = foods;
	}	
	public Customer(Long id, String fname, String lname, String email, String password, String address, String city,
			int contact_no) {
		super();
		this.id = id;
		Fname = fname;
		Lname = lname;
		this.email = email;
		this.password = password;
		Address = address;
		this.city = city;
		this.contact_no = contact_no;
	}
	
		
		public Customer(String fname, String lname, String email, String password, String address, String city) {
		super();
		Fname = fname;
		Lname = lname;
		this.email = email;
		this.password = password;
		Address = address;
		this.city = city;
	}
		
		public Set<Role> getRoles() {
			return roles;
		}
		
		public void setRoles(Set<Role> roles) {
			this.roles = roles;
		}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFname() {
		return Fname;
	}

	public void setFname(String fname) {
		Fname = fname;
	}

	public String getLname() {
		return Lname;
	}

	public void setLname(String lname) {
		Lname = lname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public int getContact_no() {
		return contact_no;
	}

	public void setContact_no(int contact_no) {
		this.contact_no = contact_no;
	}	
	public List<Food> getFoods() {
		return foods;
	}
	public void setFoods(List<Food> foods) {
		this.foods = foods;
	}


	public List<Desert> getDeserts() {
		return deserts;
	}

	public void setDeserts(List<Desert> deserts) {
		this.deserts = deserts;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", Fname=" + Fname + ", Lname=" + Lname + ", email=" + email + ", password="
				+ password + ", Address=" + Address + ", city=" + city + ", contact_no=" + contact_no + ", foods="
				+ foods + ", deserts=" + deserts + "]";
	}
	
	
}
