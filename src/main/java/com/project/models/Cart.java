package com.project.models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "carts")
public class Cart {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cat_id;

	/*
	 * @Column(name = "cust_id") private Long custId;
	 * 
	 * @Column(name = "desert_id") private Long desertId;
	 */
	@ManyToOne
	@JoinColumn(name = "desert_id")
	private Desert desert;

	private int quntity;

	@Column(name = "created_date")
	private Date createdDate;

	@ManyToOne
	@JoinColumn(name = "cust_id")
	private Customer customer;

	@OneToMany
	@JoinColumn(name = "order_id")
	private List<Food> food;

	public Cart() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * public Cart(CartDto cartDto, Desert desert,Long custId) {
	 * 
	 * this.custId = custId; this.desertId=cartDto.getDesertId();
	 * this.quntity=cartDto.getQuantity(); this.desert = desert;
	 * this.createdDate=new Date();
	 * 
	 * } public Cart(Long custId, Long desertId, int quntity) {
	 * 
	 * this.custId = custId; this.desertId = desertId; this.createdDate=new Date();
	 * this.quntity = quntity;
	 * 
	 * } public Cart(CartDto cartDto,Desert desert) { this.desertId
	 * =cartDto.getDesertId(); this.quntity =cartDto.getQuantity(); this.desert =
	 * desert; this.createdDate=new Date(); }
	 * 
	 * public Cart(AddToCartDto addToCartDto, Long custId) { this.custId = custId;
	 * this.desertId =addToCartDto.getDesertId(); this.quntity
	 * =addToCartDto.getQuantity(); this.createdDate=new Date(); }
	 */

	public Long getCat_id() {
		return cat_id;
	}

	public void setCat_id(Long cat_id) {
		this.cat_id = cat_id;
	}

	public Desert getDesert() {
		return desert;
	}

	public void setDesert(Desert desert) {
		this.desert = desert;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<Food> getFood() {
		return food;
	}

	public void setFood(List<Food> food) {
		this.food = food;
	}

	public int getQuntity() {
		return quntity;
	}

	public void setQuntity(int quntity) {
		this.quntity = quntity;
	}

	/*
	 * public Long getCustId() { return custId; }
	 * 
	 * public void setCustId(Long custId) { this.custId = custId; }
	 * 
	 * public Long getDesertId() { return desertId; }
	 * 
	 * public void setDesertId(Long desertId) { this.desertId = desertId; }
	 */
}
