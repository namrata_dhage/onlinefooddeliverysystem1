package com.project.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.project.models.Desert;
import com.project.repositorys.DesertRepository;
import com.project.services.DesertService;

@Controller
public class DessertController {

	@Autowired
	private DesertService desertService;


	@GetMapping("/restrohome")
	public String viewFoodHome() {
		return "RestroHome";
	}

	@GetMapping("/desertlist")
	public String desertList(Model model, @Param("keyword") String keyword) {
		List<Desert> desetlist = desertService.getAllDesertList(keyword);
		model.addAttribute("deserts", desetlist);
		model.addAttribute("keyword", keyword);
		return "DesertList";
	}

	@GetMapping("/adddesert")
	public String addDesert(Model model) {
		Desert desert = new Desert();
		model.addAttribute("deserts", desert);
		return "AddDesert";
	}

	/*
	 * public static String uploadDirectory = System.getProperty("user.dir") +
	 * "/uploads";
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveDesert(@ModelAttribute("desert") Desert desert, Model model,
			@RequestParam("fileimage") MultipartFile multipartFile) throws IOException {

		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		desert.setImages(fileName);
		Desert saveddes = desertService.save(desert);

		String uploadDir = "./desertImg-photos/" + saveddes.getDes_id();

		/* */
		Path uploadPath = Paths.get(uploadDir);

		if (!Files.exists(uploadPath)) {
			Files.createDirectories(uploadPath);

		}
		try (InputStream inputStream = multipartFile.getInputStream()) {
			Path filePath = uploadPath.resolve(fileName);
			System.out.println(filePath.toFile().getAbsolutePath());
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			throw new IOException("could not save the uploaded file: " + fileName);
		}
		return "redirect:/desertlist";
	}

	@RequestMapping("/delete/{id}")
	public String deleteDesert(@PathVariable(name = "id") Long id) {
		desertService.delete(id);
		System.out.println("item is deleted" + id);
		return "redirect:/desertlist";
	}

	@RequestMapping("/edit/{id}")
	public ModelAndView showEditProductPage(@PathVariable(name = "id") Long id) {
		ModelAndView mav = new ModelAndView("EditDeserts");
		Desert desert = desertService.getById(id);
		mav.addObject("deserts", desert);

		return mav;
	}
	/*
	 * @RequestMapping("/edit/{id}") public String
	 * showFormForUpdate(@PathVariable(value = "id") long id, Model model) {
	 * 
	 * // get employee from the service Desert desert =
	 * desertRepository.findById(id).get();
	 * 
	 * // set employee as a model attribute to pre-populate the form
	 * model.addAttribute("deserts", desert); return "AddDesert"; }
	 */
//	  @PostMapping("/edit") 
//	  public String updateDesert1(@ModelAttribute("desert")
//	  Desert desert) {
//	  
//	  System.out.println("desert update successfully" + desert.getDes_id()); 
//	  Desert desert2=desertService.UpdateDesert(desert);
//	  return "redirect:/desertlist"; 
//	  }

	/*
	 * @RequestMapping("/findbyId")
	 * 
	 * @ResponseBody public Optional<Desert> findById(Long id){ return
	 * desertService.getById(id); }
	 * 
	 */
	/*
	 * @RequestMapping(value = "/edit", method = { RequestMethod.PUT,
	 * RequestMethod.GET }) public String updateDeserts(Desert desert) {
	 * desertService.save(desert); return "redirect:/desertlist";
	 * 
	 * }
	 */

	/*
	 * @GetMapping("/edit/{id}") public ModelAndView updateDesert(@PathVariable(name
	 * = "id") Long id) { ModelAndView modelAndView = new
	 * ModelAndView("EditDeserts"); Desert desert = desertService.get(id);
	 * modelAndView.addObject("deserts", desert); return modelAndView; }
	 */

	/*
	 * @PostMapping("update/{id}") public String updateStudent(@PathVariable("id")
	 * long id, @Valid Student student, BindingResult result, Model model) { if
	 * (result.hasErrors()) { student.setId(id); return "update-student"; }
	 * 
	 * studentRepository.save(student); model.addAttribute("students",
	 * studentRepository.findAll()); return "index"; }
	 */

	/*
	 * @GetMapping("/edit/{id}") public String updateDesert(@PathVariable(name=
	 * "id") Long id,Model model) { Optional<Desert> des =
	 * this.desertRepository.findById(id); model.addAttribute("deserts", des);
	 * return "EditDeserts"; }
	 */
	/*
	 * @PostMapping("/edits/{id}") public String update(@PathVariable("id") Long id,
	 * Desert desert, BindingResult result, Model model) { if(result.hasErrors()) {
	 * desert.setDes_id(id); return "EditDeserts"; } return "redirect:/desertlist";
	 * }
	 */
	/*
	 * @GetMapping("/edit") public String updateDesert() { return "DesertList"; }
	 */
}
