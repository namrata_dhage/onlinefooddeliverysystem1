package com.project.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.project.models.Desert;
import com.project.models.Food;
import com.project.repositorys.FoodRepository;
import com.project.services.FoodServices;

@Controller
public class FoodController {

	@Autowired
	private FoodServices foodService;
	
	@GetMapping("/home")
	public String viewHomePage() {

		return "CustHome";
	}

	@GetMapping("/foodlist")
	public String foodList(Model model, @Param("keyword") String keyword) {
		List<Food> foodlist = foodService.getAllFoodList(keyword);
		model.addAttribute("foods", foodlist);
		model.addAttribute("keyword", keyword);
		return "FoodList";
	}

	@GetMapping("/addfood")
	public String addFood(Model model) {
		Food food = new Food();
		model.addAttribute("foods", food);
		return "AddFood";
	}

	@PostMapping("/saves")
	public String SaveFood(@ModelAttribute("food") Food food, 
			@RequestParam("filesImage") MultipartFile multipartFile) throws IOException {
		
		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		food.setImage(fileName);
		Food saveddes = foodService.save(food);
		 
	        String uploadDir = "./foodImg-photos/" + saveddes.getFood_id();
	 
			 foodService.save(food); 
			 Path uploadPath = Paths.get(uploadDir);
			 
			 if(!Files.exists(uploadPath)){
				 Files.createDirectories(uploadPath);
				 
			 }
			 try(InputStream inputStream = multipartFile.getInputStream()){
			  Path filePath =uploadPath.resolve(fileName);
			  System.out.println(filePath.toFile().getAbsolutePath());
			  Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
			 }catch (IOException e) {
				throw new IOException("could not save the uploaded file: " +fileName);
			}		
		return "redirect:/foodlist";
	}

	@GetMapping("/deleted/{id}")
	public String deleteFood(@PathVariable(name = "id") Long id) {
		foodService.delete(id);
		return "redirect:/foodlist";
	}

	@GetMapping("/update/{id}")
	public ModelAndView updateDesert(@PathVariable(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("EditFood");
		Food food = foodService.get(id);
		modelAndView.addObject("foods", food);
		return modelAndView;
	}
	
	/*
	 * @PostMapping("/update/{id}") public String
	 * updateFood(@PathVariable(name="id") Long id) { foodRepository.findById(id);
	 * return "redirect:/foodlist"; }
	 */
}
