package com.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.project.models.Cart;
import com.project.models.Customer;
import com.project.models.Desert;
import com.project.models.Food;
import com.project.services.CartService;
import com.project.services.DesertService;
import com.project.services.FoodServices;

@Controller
public class CartController {

	@Autowired
	private CartService cartService;
	@Autowired
	private DesertService desertService;
	@Autowired
	private FoodServices foodServices;
	
	@GetMapping("/addCart/{id}")
	public ModelAndView AddCart(@PathVariable(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("Cart");
		Desert desert = desertService.getById(id);
		modelAndView.addObject("deserts", desert);
		return modelAndView;
	}
	
	/*
	 * @GetMapping("/addcart1") public String addDesert(Model model) { Cart cart
	 * =new Cart(); model.addAttribute("carts", cart); return "AddDesert"; }
	 * 
	 * @RequestMapping("/addcart2") public String viweOrder(Model model, Cart cart)
	 * {
	 * 
	 * cartService.save(cart);
	 * 
	 * return "Cart"; }
	 */
	@RequestMapping("/addcarts/{id}")
	public String AddCart1(@PathVariable(name = "id") Long id,Model model) {
		System.out.println("item is add in cart"+id);
		Cart cart= new Cart();
		  List<Food> food =cart.getFood();
		  cartService.save(cart);
		  model.addAttribute("foods", food); 
		return "redirect:/foodList1";
	}
	
	@GetMapping("/cart")
	public String AddCartItem(Model model) {	
		Customer customer =new Customer();
		List<Food> cartlist =customer.getFoods();
		model.addAttribute("foods", cartlist);
		model.addAttribute("pageTitle", "Shopping Cart");

		return "Cart";
	}
}
