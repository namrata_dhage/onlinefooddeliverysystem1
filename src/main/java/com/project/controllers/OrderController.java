package com.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.models.Customer;
import com.project.models.Desert;
import com.project.models.Food;
import com.project.models.OrderList;
import com.project.repositorys.CustomerRepository;
import com.project.repositorys.OrderRepository;
import com.project.services.DesertService;
import com.project.services.FoodServices;
import com.project.services.OrderService;

@RestController
public class OrderController {

	@Autowired
	private OrderService orderService;


	@Autowired
	private DesertService desertService;

	@Autowired
	private FoodServices foodService;


	@GetMapping("/orderlist")
	public String orderList(Model model) {
		
		  List<OrderList> orderlist = orderService.getOrderList();
		  model.addAttribute("orders", orderlist);
		 
		return "OrderList";
	}

	@GetMapping("/order/{id}")
	public ModelAndView orderDesert(@PathVariable(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("Order");
		Desert desert = desertService.getById(id);
		modelAndView.addObject("deserts", desert);
		return modelAndView;
	}

	@GetMapping("/orders/{id}")
	public ModelAndView orderfood(@PathVariable(name = "id") Long id) {
		ModelAndView modelAndView = new ModelAndView("OrderFood");
		Food food = foodService.get(id);
		modelAndView.addObject("foods", food);
		return modelAndView;
	}
	
	/*
	 * @GetMapping("/orderplace/{id}") public ModelAndView
	 * placedOrder(@PathVariable(name="id")Long id) { ModelAndView modelAndView =
	 * new ModelAndView("OrderList"); OrderList orderList =orderService.get(id);
	 * modelAndView.addObject("orders",orderList); return modelAndView; }
	 */
	
	  
	  @RequestMapping("/orderplace1") 
	  public String newOrderPlace( Model model) {
		  
			/*
			 * List<Desert> desert = desertService.findAll(); List<Food> food =
			 * foodService.findAll();
			 */
			/* System.out.println("orderList here="+desert.toString()); */
	  model.addAttribute("orderlists", new OrderList());
	  
		/* model.addAttribute("deserts", desert); */
	  
	  return "orderPlace";
	  }
	 
}
