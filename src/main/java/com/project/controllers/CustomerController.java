package com.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.project.models.Customer;
import com.project.models.Desert;
import com.project.models.Food;
import com.project.repositorys.CustomerRepository;
import com.project.services.DesertService;
import com.project.services.FoodServices;

@Controller
public class CustomerController {

	@Autowired
	private CustomerRepository custRepository;

	@Autowired
	private DesertService desertService;

	@Autowired
	private FoodServices foodService;

	@GetMapping("/")
	public String viweHomePage() {
		return "index";
	}

	@GetMapping("/login")
	public String getLogin(Model model) {
		model.addAttribute("customer", new Customer());

		return "login";
	}

	@PostMapping("/process_login")
	public String processLogin(@ModelAttribute(name = "customer") Customer customer, Model model) {
		String email = customer.getEmail();
		String password = customer.getPassword();
		
		if ("admin".equals(email) && "admin".equals(password)) {
			return "RestroHome";
		} else if (email.equals(email) && password.equals(password)) {
			return "CustHome";
		}
		return "login";
	}

	@GetMapping("/register")
	public String showRegistrationForm(Model model) {

		model.addAttribute("customer", new Customer());

		return "registerForm";
	}

	@PostMapping("/success_register")
	public String processRegister(@ModelAttribute("customer") Customer customer) {
		
		custRepository.save(customer);

		return "login";
	}

	@GetMapping("/desertList1")
	public String desertList1(Model model, @Param("keyword") String keyword) {
		List<Desert> desetlist = desertService.getAllDesertList(keyword);
		model.addAttribute("deserts", desetlist);
		model.addAttribute("keyword", keyword);
		return "CustDesertList";
	}

	@GetMapping("/foodList1")
	public String foodList1(Model model, @Param("keyword") String keyword) {
		List<Food> foodlist = foodService.getAllFoodList(keyword);
		model.addAttribute("foods", foodlist);
		model.addAttribute("keyword", keyword);
		return "CustFoodList";

	}
	

}
