package com.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.models.OrderList;
import com.project.repositorys.OrderRepository;

@Service
public class OrderService {

	@Autowired 
	private OrderRepository orderRepository;
	
	public List<OrderList> getOrderList(){
			return orderRepository.findAll();
	}
	public void save(OrderList orderList) {
		orderRepository.save(orderList);
	}
	public OrderList get(Long id) {
		return orderRepository.findById(id).get();
	}
}
