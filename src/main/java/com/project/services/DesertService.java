package com.project.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.models.Desert;
import com.project.repositorys.DesertRepository;

@Service
public class DesertService {

	@Autowired
	private DesertRepository desertRepository;
	
	public List<Desert> getAllDesertList(String keyword)
	{
		if(keyword != null) {
			return desertRepository.findAll(keyword);
		}
		return desertRepository.findAll();
	}
	public Desert save(Desert desert) {
	    return desertRepository.save(desert);
	}
	
	public Desert getById(Long id) {
        return desertRepository.findById(id).get();
    }
     	
    public void delete(Long id) {
    	desertRepository.deleteById(id);
    }

	/*
	 * public List<Desert> findAll() { // TODO Auto-generated method stub return
	 * desertRepository.findAll(); }
	 */
	public List<Desert> findAll() {
		// TODO Auto-generated method stub
		return desertRepository.findAll();
	}

	
}
