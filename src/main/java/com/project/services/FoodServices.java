package com.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.models.Desert;
import com.project.models.Food;
import com.project.repositorys.FoodRepository;

@Service
public class FoodServices {

	@Autowired
	private FoodRepository foodRepository;
	
	public List<Food> getAllFoodList(String keyword)
	{
		if(keyword != null) {
			return foodRepository.findAll(keyword);
		}
		return foodRepository.findAll();
	}
	public Food save(Food food) {
		return foodRepository.save(food);
	}
	public void delete(Long id) {
    	foodRepository.deleteById(id);
    }
	public Food get(Long id) {
        return foodRepository.findById(id).get();
    }
	public List<Food> findAll() {	
		return foodRepository.findAll();
	}
	
	
}
