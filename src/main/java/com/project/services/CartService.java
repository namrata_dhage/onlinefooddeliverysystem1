package com.project.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.models.Cart;

import com.project.models.Customer;

import com.project.repositorys.CartRepository;

@Service
@Transactional
public class CartService {

	
	private final CartRepository cartRepository;
	
	public CartService(CartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }
	
	/*
	 * public void addToCart(AddToCartDto addToCartDto,Long custId){ Cart cart =
	 * getAddToCartFromDto(addToCartDto,custId); cartRepository.save(cart); }
	 * 
	 * private Cart getAddToCartFromDto(AddToCartDto addToCartDto, Long custId) {
	 * Cart cart = new Cart(addToCartDto, custId); return cart; }
	 * 
	 * public cartCost listCartItems(Long custId) { List<Cart> cartList =
	 * cartRepository.findByAllcustIdOrderBycreatedDate(custId); List<CartDto>
	 * cartItems = new ArrayList<>(); for (Cart cart:cartList){ CartDto cartDto =
	 * getDtoFromCart(cart); cartItems.add(cartDto); } double totalCost = 0; for
	 * (CartDto cartDto:cartItems){ totalCost += (cartDto.getDesert().getPrice()*
	 * cartDto.getQuantity()); } cartCost cartcost = new
	 * cartCost(cartItems,totalCost); return cartcost; }
	 * 
	 * 
	 * 
	 * private CartDto getDtoFromCart(Cart cart) { CartDto cartDto = new
	 * CartDto(cart); return cartDto; }
	 */

	public List<Cart> getAllDetails(){
		return cartRepository.findAll();
	}

	/*
	 * public List<Cart> getAllCustomerOrder(List<Customer> customer){ return
	 * cartRepository.findByCustomer(customer); }
	 */
	public void save(Cart cart) {	
		cartRepository.save(cart);
	}

	public static List<Cart> findAll() {
	
		return null;
	}
	
}
