package com.project.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
	
	
}
