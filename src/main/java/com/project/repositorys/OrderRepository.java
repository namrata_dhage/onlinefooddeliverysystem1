package com.project.repositorys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.models.Desert;
import com.project.models.OrderList;

@Repository
public interface OrderRepository extends JpaRepository<OrderList, Long> {
	
	/*
	 * @Query(SELECT d.desert_name,d.category,d.price,d.images, c.fname FROM Desert
	 * d JOIN Customer c ON d.cust_id =c.id WHERE d.cust_id=?);
	 */
	
	
}
