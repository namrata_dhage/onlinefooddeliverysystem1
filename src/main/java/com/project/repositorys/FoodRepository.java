package com.project.repositorys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.models.Desert;
import com.project.models.Food;

@Repository
public interface FoodRepository extends JpaRepository<Food, Long>{

	@Query("SELECT f FROM Food f WHERE f.food_name LIKE %?1%"
			+" OR f.category LIKE %?1% ")
	public List<Food> findAll(String keyword);

	
}
