package com.project.repositorys;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.models.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>{
	
	
}
