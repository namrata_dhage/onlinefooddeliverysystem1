package com.project.repositorys;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.models.Desert;

@Repository
public interface DesertRepository extends JpaRepository<Desert, Long>{

	@Query("SELECT d FROM Desert d WHERE d.Desert_name LIKE %?1%"
			+" OR d.category LIKE %?1% ")
	public List<Desert> findAll(String keyword);
	
 

}
